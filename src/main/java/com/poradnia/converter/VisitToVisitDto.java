package com.poradnia.converter;

import org.springframework.stereotype.Component;

import com.poradnia.model.Visit;
import com.poradnia.model.VisitDto;

@Component
public class VisitToVisitDto {

	public VisitDto convert(Visit visit) {
		VisitDto visitDto = new VisitDto();
		visitDto.setEndDate(visit.getEndDate());
		visitDto.setStartDate(visit.getStartDate());
		visitDto.setTitle(visit.getTitle());
		visitDto.setId(visit.getId());
		return visitDto;

	}
}
