package com.poradnia.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.poradnia.converter.VisitToVisitDto;
import com.poradnia.model.VisitDto;
import com.poradnia.repository.VisitRepository;

@Controller
public class ScheduleController {

	@Autowired
	VisitToVisitDto converter;

	@Autowired
	VisitRepository visitRepositor;

	@RequestMapping(value = "/harmonogram", method = RequestMethod.GET)
	public String visits() {
		return "harmonogram";
	}

	@RequestMapping(value = "/wizyty", produces = { MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.GET)
	public ResponseEntity<List<VisitDto>> getVisits() {
		List<VisitDto> visitsDto = visitRepositor.findByPsychologistId(1L).stream().map(v -> converter.convert(v))
				.collect(Collectors.toList());
		return new ResponseEntity<List<VisitDto>>(visitsDto, HttpStatus.OK);
	}

	
	
}
