package com.poradnia.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.poradnia.model.Image;
import com.poradnia.model.Note;
import com.poradnia.model.User;
import com.poradnia.repository.NoteRepository;
import com.poradnia.repository.PsychologistRepository;
import com.poradnia.repository.UserRepository;

@RequestMapping(value = "/uzytkownicy")
@Controller
public class UsersController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	PsychologistRepository psychologistRepository;

	@Autowired
	NoteRepository noteRepository;

	@RequestMapping(method = RequestMethod.GET)
	public String users(Model model, @RequestParam(value = "search", required = false) String searchString) {
		List<User> users = userRepository.findAll();
		List<User> findUsers = new ArrayList<>();
		if (searchString!=null) {
			findUsers = users.stream()
					.filter(u -> u.getLastname().contains(searchString) || u.getFirstname().contains(searchString))
					.collect(Collectors.toList());
		}
		if (findUsers.isEmpty()) {
			model.addAttribute("users", users);
		} else {
			model.addAttribute("users", findUsers);
		}
		model.addAttribute("notatka", new Note());
		return "uzytkownicy";
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String userDetails(Model model, @PathVariable Long id) throws IOException {
		User userById = userRepository.findOne(id);
		List<User> users = userRepository.findAll();
		List<Note> notes = userById.getNotes();
		final BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(userById.getImage().getImage()));
		model.addAttribute("users", users);
		model.addAttribute("details", userById);
		model.addAttribute("image", bufferedImage);
		model.addAttribute("notes", notes);
		model.addAttribute("notatka", new Note());
		return "uzytkownicy";

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	public String addNote(@PathVariable Long id, Note notatka) {
		Note note = new Note();
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		Date date = new Date();
		note.setDate(dateFormat.format(date));
		note.setUserNotes(userRepository.findOne(id));
		note.setNoteText(notatka.getNoteText());
		noteRepository.save(note);
		return "redirect:" + id;
	}

	@RequestMapping(value = "/image/{userId}", produces = MediaType.IMAGE_PNG_VALUE)
	public ResponseEntity<byte[]> getImage(@PathVariable("userId") Long userId) throws IOException {
		User user = userRepository.findOne(userId);
		Image image =user.getImage();
		byte[] imageContent = image.getImage();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_PNG);
		return new ResponseEntity<byte[]>(imageContent, headers, HttpStatus.OK);
	}


}
