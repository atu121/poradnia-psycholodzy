package com.poradnia.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poradnia.model.Visit;

public interface VisitRepository extends JpaRepository<Visit, Long> {
	List<Visit> findByUserLogin(String name);

	List<Visit> findByPsychologistId(Long id);
}
