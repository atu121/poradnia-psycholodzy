package com.poradnia.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poradnia.model.Note;

public interface NoteRepository extends JpaRepository<Note, Long> {

}
