package com.poradnia.model;

import java.util.List;

public class EventsDto {

	private List<VisitDto> visitList;

	public List<VisitDto> getVisitList() {
		return visitList;
	}

	public void setVisitList(List<VisitDto> visitList) {
		this.visitList = visitList;
	}

}
